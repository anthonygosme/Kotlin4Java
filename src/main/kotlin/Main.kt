package mykotlin

import mykotlin.Color.*

open class Parent
class Child : Parent()

enum class Color { BLUE, ORANGE, RED, GREEN }

@JvmOverloads
fun sumkt(a: Int = 0, b: Int = 0, c: Int = 0) {// test code
}

fun getcolor(color: Color): Color {
    when (color) {
        BLUE, GREEN -> "cold"
        else -> "caca d'oie"
    }
    val c1 = ""
    val c2 = ""
    val response = if (setOf(c1, c2) == setOf(c2, c1)) "same color" else "diferente color"
    return RED
}

fun String.get(i : Int) ="x"


fun checkType(): String =
    when (getcolor(RED)) {
        is Color -> "YEEES"
    }

fun loops() {
    val map = mapOf(1 to "one", 2 to "two")
    for ((key, value) in map) {
        for (i in 1..9){
            for (i in 1..9){}
        }
    }

}


fun isValidIdentifier(s: String): Boolean {
    var firstchar = true
    if (s == "") return false
    for (c in s)
    {
        when (firstchar) {
            true ->
                if (c !in 'a'..'z' && c != '_' ) return false
            false ->
                if (c !in 'a'..'z'&& c !in '0' .. '9' && c != '_' ) return false
        }
        firstchar = false
    }

    return true
}

fun String.LastChar() = get(length -1)

//fun sum(list: List<Int>): Int {
fun List<Int>.sum(): Int {
    var result = 0
    for (i in this) {
        result += i
    }
    return result
}
fun CallList(){
    val sum = listOf(1, 2, 3).sum()
    println(sum)    // 6
}

fun inside(){
    val c ='b'
    if (c in 'a' until 'z'  ) "yes"

}
fun main(args: Array<String>) {
    println("Hello World!")
    sumkt(10, 10)
    sumkt(c = 10)
    getcolor(RED)
    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")
    println("abs"[1])
}